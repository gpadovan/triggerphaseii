library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector
use ieee.math_real.uniform; -- to extract uniform random numbers in [0,1)
use ieee.math_real.floor; -- to do floor (parte intera) operation
use ieee.numeric_std.all;


entity top_tb is
--  Port ( );
end top_tb;

architecture Behavioral of top_tb is
    
    -- input signals from test bench to top
    -- clock signals
    signal tb_clock_400MHz : std_logic;
    signal tb_clock_40MHz : std_logic;    
    -- control signals
    signal tb_reset : std_logic;
    signal tb_write_en : std_logic;
    signal tb_read_en : std_logic;
    -- data signals
    signal tb_hit : std_logic_vector (31 downto 0) := (others => '0');
    signal tb_bcid : std_logic_vector (11 downto 0) := (others => '0');
    signal tb_data_flow : std_logic_vector (31 downto 0) := (others=> '0');
    signal tb_hit_counter : std_logic_vector (3 downto 0) := (others => '0');
    
    -- output signals from top to test bench
    signal tb_out : std_logic_vector (31 downto 0);
    signal tb_out_count : std_logic_vector (11 downto 0);
    
    -- varaibales for random number generation          -- DOMANDA: e' corretto mettere qui queste variabili? perche' shared?
    shared variable seed_1 : integer := 1;
    shared variable seed_2 : integer := 1;
    shared variable rand_real : real;
    shared variable rand_int : integer;
    
    -- signal to report in test banch extracted random number in [0,9]
    signal tb_rand_int : std_logic_vector (3 downto 0) := (others => '0');


    
    component top
        Port (clock_400MHz : in std_logic;
              clock_40MHz : in std_logic;
              reset : in std_logic;
              write_en : in std_logic;
              read_en : in std_logic;
              data_in : in std_logic_vector (31 downto 0);
              top_out : out std_logic_vector (31 downto 0);
              top_out_count : out std_logic_vector (11 downto 0) );
    end component;
    
    
begin
    my_sim : top port map (
        clock_400MHz => tb_clock_400MHz,
        clock_40MHz => tb_clock_40MHz,
        reset => tb_reset,
        write_en => tb_write_en,
        read_en => tb_read_en,
        data_in => tb_data_flow,
        top_out => tb_out,
        top_out_count => tb_out_count);
        
    -- clock generation processes  
    clock_400MHz_gen : process
    begin
        tb_clock_400MHz <= '1';
        wait for 1.25 ns;
        tb_clock_400MHz <= '0';
        wait for 1.25 ns;
    end process;
        
    clock_40MHz_gen : process
    begin
        tb_clock_40MHz <= '1';
        wait for 12.5 ns;
        tb_clock_40MHz <= '0';
        wait for 12.5 ns;
    end process;

    -- control signal generation processes
    reset_gen : process
    begin
      tb_reset <= '0';
      wait for 5 ns;
      tb_reset <= '1';
      wait for 500 ns;
      tb_reset <= '0';
      wait;
    end process;

    writeRead_en_gen : process
    begin
      tb_write_en <= '0';
      tb_read_en <= '0';
      wait for 1000 ns;
      tb_write_en <= '1';
      wait for 100 ns;
      tb_read_en <= '1';
      wait;
    end process;

    -- hit generation process
    hit_gen : process(tb_clock_400MHz)
    begin
        if rising_edge(tb_clock_400MHz) then
            tb_hit <= tb_hit + 5; -- generate counter
        end if;
    end process;
        
     -- BCID generation process
    bcid_gen : process(tb_clock_40MHz)
    begin
        if rising_edge(tb_clock_40MHz) then
            tb_bcid <= tb_bcid + 1;
        end if;
    end process;
            
    -- data flow generation process
    data_flow_gen : process(tb_clock_400MHz)
    begin
        if rising_edge(tb_clock_400MHz) then
            if (tb_hit_counter = x"0") then
                -- generate header pkg.
                tb_data_flow <= '1' & "0000000000000000000" & tb_bcid;
                -- generate number of hit packages that should follow the header (min 0, max 9)
                uniform(seed_1, seed_2, rand_real);
                rand_int := integer(floor(rand_real*10.0)); -- random integer in the interval [0,9]
                tb_rand_int <= std_logic_vector(to_unsigned(rand_int,tb_rand_int'length)); -- send signal to test bench reporting random number
            elsif( tb_hit_counter > x"0" and tb_hit_counter <= rand_int) then
                -- generate hits pkgs.
                tb_data_flow <= '0' & tb_hit(30 downto 0);
            else
                tb_data_flow <= (others => '0'); -- do nothing
            end if; 
        end if;

    end process;
        
    -- counter for number of hit in a single bunch crossing
    hit_counter_gen : process(tb_clock_400MHz)
    begin
        if rising_edge(tb_clock_400MHz) then
            if tb_hit_counter = 9 then
                tb_hit_counter <= (others => '0');
            else 
                tb_hit_counter <= tb_hit_counter + 1;
            end if;
        end if;
    end process;
    
end Behavioral;
