-- library declaration
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector

-- entity
entity fsm is
    Port ( TOG_EN : in std_logic;
           CLK, CLR : in std_logic;
           Y, Z1 : out std_logic);
end fsm;

-- architecture
architecture Behavioral_fsm of fsm is
    type state_type is (ST0, ST1);
    signal PS, NS : state_type;
begin
    sync_proc : process(CLK)
    begin
        if rising_edge(CLK) then
            if (CLR = '1') then
                PS <= ST0;
            else    
                PS <= NS;
            end if;
        end if;
    end process sync_proc;

    comb_proc : process(PS, TOG_EN)
    begin
        Z1 <= '0';
        case PS is
            when ST0 =>
                Z1 <= '0';
                if (TOG_EN = '1') then NS <= ST1;
                else NS <= ST0;
                end if;
            when ST1 =>
                Z1 <= '1';
                if (TOG_EN = '1') then NS <= ST0;
                else NS <= ST1;
                end if;
            when others =>
                Z1 <= '0';
                NS <= ST0;
        end case;
    end process comb_proc;
    
    -- assign values representing the state variables
    with PS select
        Y <= '0' when ST0,
             '1' when ST1,
             '0' when others;
    
end Behavioral_fsm;
