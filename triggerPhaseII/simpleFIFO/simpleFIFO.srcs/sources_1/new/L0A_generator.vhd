-- L0A_generator.vhd [source file for project simpleFIFO]

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector
use ieee.math_real.uniform; -- to extract uniform random numbers in [0,1)
use ieee.math_real.floor; -- to do floor (parte intera) operation


entity L0A_generator is
    Port ( clock : in std_logic;
           enable : in std_logic;
           out_flow : out std_logic );
end L0A_generator;

architecture Behavioral of L0A_generator is
    --------------------------------------
    -- define signals and variables in data_generator
    --------------------------------------
    type state_type is (ST0, ST1, idle);
    signal CS : state_type := idle; -- [non sintetizzabile]
    signal counter_blanks : std_logic_vector(18 downto 0);
    
    -- signal for random number generation
    shared variable seed_1 : integer := 1;
    shared variable seed_2 : integer := 1;
    shared variable rand_real : real;
    shared variable rand_nhits : integer;
    
    
begin
    --------------------------------------
    -- processes generating signals
    --------------------------------------  
    seq_proc : process(clock)
    begin
        if rising_edge(clock) then
            case CS is
                when idle =>
                    out_flow <= '0';
                    if enable = '1' then
                        CS <= ST0;
                    end if;
                when ST0 => 
                    out_flow <= '1';
                    uniform(seed_1, seed_2, rand_real); --prepare to send rand_nhits pkgs.
                    rand_nhits := integer(floor(rand_real*160000.0))+320000; --random integer in the interval [3.2e5 4.8e5] -- PROBLEMA: controllare bene la strategia di emissione dei pacchetti e se e' oppportuno usare una macchina a stati
                    counter_blanks <= (others => '0');
                    CS <= ST1;
                when ST1 =>
                    out_flow <= '0';
                    if counter_blanks = (rand_nhits-1) then --stop after sending rand_nhits pkgs.
                        CS <= ST0;
                    end if;
                    counter_blanks <= counter_blanks+1;   
            end case;
        end if;
        
    end process seq_proc;
                  
end Behavioral;
