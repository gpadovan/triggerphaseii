-- top.vhd [source file for project simpleFIFO]

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operations on std_logic_vector
use IEEE.numeric_std.all;
library xil_defaultlib; -- FIFO library
library work;
use work.all;


entity top is
    Port (clock_400MHz : in std_logic;
          clock_40MHz : in std_logic;
          reset : in std_logic;
          data_flow : in std_logic_vector(31 downto 0) := (others => '0');
          top_out: out std_logic_vector (31 downto 0);
          top_bcid_en : in std_logic );      
end top;

architecture Behavioral of top is
    --------------------------------------
    --  define signals in top
    --------------------------------------
    -- control signal
    signal write_en : std_logic := '0';
    signal read_en : std_logic;
    signal wait_read_en : std_logic;
    signal FIFO_empty : std_logic;
    signal FIFO_full : std_logic;

    -- data signals 
    signal top_bcid : std_logic_vector (11 downto 0) := (others => '0');
    signal data_flow_delayed : std_logic_vector (31 downto 0);   
    signal dout : std_logic_vector (31 downto 0) := (others => '0');
    signal data_count : std_logic_vector (14 downto 0) := (others => '0'); -- PROBLEMA: nel file FIFO.memory.vhd il segnale e' definito come STD_LOGIC_VECTOR(13 DOWNTO 0) 
    
    -- signals to check if the pkg is old
    signal isHeader : std_logic := '0';
    signal pkg_bcid : std_logic_vector (11 downto 0) := (others => '0');
    signal diff_bcid : std_logic_vector (11 downto 0) := (others => '0');
    signal diff_bcid2 : std_logic_vector (11 downto 0) := (others => '0');
    signal diff_bcid_NEW : std_logic_vector (11 downto 0) := (others => '0');
    
begin
    --------------------------------------
    -- component definition and port mapping
    -------------------------------------- 
    FIFO_inst : entity work.FIFO_memory
      Port map(
        wr_en => write_en,
        rd_en => read_en,
        din => data_flow_delayed,
        dout => dout,
        srst => reset,
        clk => clock_400MHz,
        full => FIFO_full,
        empty => FIFO_empty,
        data_count => data_count );

    --------------------------------------
    -- signals assignments
    -------------------------------------- 
    top_out <= dout;

    --------------------------------------
    -- processes generating signals
    --------------------------------------   
    -- generation of write_en
    write_en_gen : process (clock_400MHz)
    begin
        if rising_edge(clock_400MHz) then -- PROBLEMA: si puo' riscrivere quasta serie di if indentati con un'unica condizione
            if reset = '1' then 
                write_en <= '0';
            else
                if data_flow=x"0000" then
                    write_en <= '0';
                else
                    write_en <= '1';
                end if;
            end if;
        end if;     
    end process;
    
    -- generation of read enable signal
    wait_read_en_gen : process
    begin
        wait_read_en <= '1';
        wait for 1000 ns;
        wait_read_en <= '0';
        wait;
    end process;
    
    read_en_gen : process (clock_400MHz)
    begin
        if rising_edge(clock_400MHz) then
            if reset = '1' or wait_read_en = '1' or FIFO_empty ='1' then -- deactivate read_en if controls not passed
                read_en <= '0';
            else
                if to_integer(unsigned(diff_bcid2)) < 400 then -- latency 10 microseconds, then n_pkgs = latency / 25 ns = 400 pkgs
                    read_en <= '0'; --"young" pkg. => keep it 
                else
                    read_en <= '1'; -- "old" pkg. => throw it
                end if;
            end if;
        end if;
    end process;
    
    
    -- generation of delayed data_flow
    data_flow_delayed_gen : process (clock_400MHz)
    begin
        if rising_edge(clock_400MHz) then
            data_flow_delayed <= data_flow;
        end if;
    end process;
    
    -- generation of BCID counter signal in the top entity
    top_bcid_gen : process (clock_40MHz)
    begin
        if rising_edge(clock_40MHz) then
            if top_bcid_en = '1' then
                top_bcid <= top_bcid + 1;
            end if;
        end if;
    end process;
    
    isHeader_gen: process (clock_400MHz)
    begin
        if rising_edge(clock_400MHz) then
            isHeader <= '0';    
            if dout(31) = '1' then
                pkg_bcid <= dout(11 downto 0);
                diff_bcid <= std_logic_vector(abs(signed(top_bcid) - signed(pkg_bcid))); -- PROBLEMA: come calcolo la differenza? [questa differenza e' calcolata sul fronte di salita di isHeader, ma **forse** considera il numero di pacchetto di in uscita precedente a quello che fa scattare isHeader]
                isHeader <= '1';
            end if;
        end if;
    end process;
    
--    diff_bcid2_gen : process(clock_400MHz)
--    begin
--        if rising_edge(clock_400MHz) then
--            if isHeader = '1' then
--                diff_bcid2 <= std_logic_vector(abs(signed(top_bcid) - signed(pkg_bcid))); -- PROBLEMA: come calcolo la differenza? [questa differanza e' calcolata sul fronte di discesa di isHeader (1 clock dopo)  e considera il numero di pacchetto in uscita che ha effettivamente fatto scattare isHeader]
--                                                                                          -- effettivamente questa e' la differenza corretta, tuttavia c'e' il problema che per generarla bisogna ritardare di un altro colpo di clock per alzare la flag isHeader
--                                                                                          -- alla fine la differenza e' calcolata due colpi di clock dopo ripetto al momento in cui l'header si presenta all'uscita della FIFO
--                                                                                          -- (serve 1 colpo di clock per alzare isHeader e 1 colpo di clock per calcolare la differenza)
--            end if;
--        end if;
--    end process;

    

    
    diff_bcid_NEW_gen : process(clock_400MHz)
    begin
        if rising_edge(clock_400MHz) then
            diff_bcid_NEW <= std_logic_vector(abs(signed(top_bcid) - signed(pkg_bcid)));
        end if;
    end process;
    
    
    -- fai un processo separato per la differenza
    -- fai un altro processo che cotrolla tutto insieme
    
    --diff_bcid2_gen : process(isHeader, top_bcid)
    --begin
    --    --if rising_edge(isHeader) then                                               -- PROBLEMA: ora che ho messo top_bcid nella sentitivity list, come faccio ad imporre la condizione di essere sul fronte di salita di isHeader? [se metto il solito if, di fatto non triggero piu' il process sul segnale top_bcid] N.B.: in realta' non e' un problema tirggerare il process anche sul fronte di discesa di isHeader (il segnale differenza viene riaggiornato allo stesso valore e non cambia nulla, anche se l'operazione di riaggirnamento del segnale allo stesso valore e' inutile).
    --        diff_bcid2 <= std_logic_vector(abs(signed(top_bcid) - signed(pkg_bcid))); -- PROBLEMA: questo processo e' triggerato direttamente da isHeader e fa risparmiare un colpo di clock                                                                                          
    --    --end if;
    --end process;
    
    
end Behavioral;
