-- data_generator.vhd [source file for project simpleFIFO]

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector
use ieee.math_real.uniform; -- to extract uniform random numbers in [0,1)
use ieee.math_real.floor; -- to do floor (parte intera) operation


entity data_generator is
    Port ( clock : in std_logic;
           enable : in std_logic;
           hit_flow : in std_logic_vector(31 downto 0);
           out_flow : out std_logic_vector(31 downto 0) );
end data_generator;

architecture Behavioral of data_generator is
    --------------------------------------
    -- define signals and variables in data_generator
    --------------------------------------
    type state_type is (ST0, ST1, idle);
    signal CS : state_type := idle; -- [non sintetizzabile] 
    signal counter_hit : std_logic_vector(3 downto 0);
    signal counter_empty : std_logic_vector(3 downto 0);
    signal num_header : std_logic_vector(11 downto 0) := (others => '0');
    
    -- signal for random number generation
    shared variable seed_1 : integer := 1;
    shared variable seed_2 : integer := 1;
    shared variable rand_real : real;
    shared variable rand_real2 : real;
    shared variable rand_nhits : integer;
    
    
begin
    --------------------------------------
    -- processes generating signals
    --------------------------------------  
    seq_proc : process(clock)
    begin
        if rising_edge(clock) then
            case CS is
                when idle =>
                    out_flow <= (others => '0');
                    if enable = '1' then
                        CS <= ST0;
                        num_header <= num_header + 1;
                    end if;
                when ST0 =>
                    uniform(seed_1, seed_2, rand_real);
                    if( rand_real < 0.78) then -- previous: 0.9
                        out_flow <= (others => '0'); -- wait
                    else
                        out_flow <= '1' & "0000000000000000000" & num_header; -- send header
                        uniform(seed_1, seed_2, rand_real2); --prepare to send rand_nhits pkgs.
                        rand_nhits := integer(floor(rand_real2*10.0))+1; --random integer in the interval [1,10]
                        counter_hit <= "0000";
                        num_header <= num_header + 1;
                        CS <= ST1;
                    end if;
                when ST1 =>
                    out_flow <= '0' & hit_flow(30 downto 0); --send hits
                    if counter_hit = (rand_nhits-1) then --stop after sending rand_nhits pkgs.
                        CS <= ST0;
                    end if;
                    counter_hit <= counter_hit+1;
            end case;
        end if;
        
    end process seq_proc;

end Behavioral;
