-- top_tb.vhd [source file for project simpleFIFO]

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector

use ieee.math_real.uniform; -- to extract uniform random numbers in [0,1)
use ieee.math_real.floor; -- to do floor (parte intera) operation
use ieee.numeric_std.all;


entity top_tb is
--  Port ( );
end top_tb;

architecture Behavioral of top_tb is
    --------------------------------------
    --  define signals in top_tb
    --------------------------------------
    -- clock signals
    signal tb_clock_400MHz : std_logic;
    signal tb_clock_40MHz : std_logic;    
    signal tb_clock_1KHz : std_logic;
    -- control signals
    signal tb_reset : std_logic;
    signal gen_en : std_logic;
    -- data signals
    signal tb_hit : std_logic_vector (31 downto 0) := (others => '0');
    -- output signals
    signal tb_out : std_logic_vector (31 downto 0);
    signal dg_out : std_logic_vector (31 downto 0);
    signal L0A : std_logic := '0';

    --------------------------------------
    -- define components in top_tb
    --------------------------------------   
    -- data generator component (finite-state-machine)
    component data_generator
        Port ( clock : in std_logic;
               enable : in std_logic;
               hit_flow : in std_logic_vector(31 downto 0);
               out_flow : out std_logic_vector(31 downto 0) );
    end component;
    
    -- top component (sector logic)
    component top
        Port (clock_400MHz : in std_logic;
              clock_40MHz : in std_logic;
              reset : in std_logic;
              data_flow : in std_logic_vector(31 downto 0);
              top_out : out std_logic_vector (31 downto 0);
              top_bcid_en : in std_logic);
    end component;
    
    -- L0A generator component 
    component L0A_generator -- PROBLEMA: chiedere conferma che il L0A generator e' esterno alla sector logic [forse a ben guardare andrebbe messo interno...]
        Port ( clock : in std_logic;
               enable : in std_logic;
               out_flow : out std_logic );
    end component;
    
   
begin
    --------------------------------------
    -- components port mapping
    --------------------------------------   
    -- port mapping for data generator component    
    pmap_data_generator : data_generator port map (
        clock => tb_clock_400MHz,
        enable => gen_en,
        hit_flow => tb_hit,
        out_flow => dg_out );
    
    -- port mapping for top component
    pmap_top : top port map (
        clock_400MHz => tb_clock_400MHz,
        clock_40MHz => tb_clock_40MHz,
        reset => tb_reset,
        data_flow => dg_out,
        top_out => tb_out,
        top_bcid_en => gen_en );
   
    -- port mapping for L0A generator component
    pmap_L0A_generator : L0A_generator port map (
        clock => tb_clock_400MHz,
        enable => gen_en,
        out_flow => L0A );
    
    
    --------------------------------------
    -- processes generating signals
    --------------------------------------        
    -- generation of clock at 400 MHz
    clock_400MHz_gen : process
    begin
        tb_clock_400MHz <= '1';
        wait for 1.25 ns;
        tb_clock_400MHz <= '0';
        wait for 1.25 ns;
    end process;
    
    -- generation of clock at 40 MHz
    clock_40MHz_gen : process
    begin
        tb_clock_40MHz <= '1';
        wait for 12.5 ns;
        tb_clock_40MHz <= '0';
        wait for 12.5 ns;
    end process;
    
    -- generation of clock at 10 KHz
    clock_1KHz_gen : process
    begin
        tb_clock_1KHz <= '1';
        wait for 0.5 ms;
        tb_clock_1KHz <= '0';
        wait for 0.5 ms;
    end process;

    -- generation of reset signal
    reset_gen : process
    begin
      tb_reset <= '1';
      wait for 100 ns;
      tb_reset <= '0';
      wait;
    end process;

    -- generation of data-generator-enalbe signal
    gen_en <= '0', '1' after 500 ns;

    -- generation of hit data
    tb_hit_gen : process(tb_clock_400MHz)
    begin
        if rising_edge(tb_clock_400MHz) then
            tb_hit <= tb_hit + 5; -- generate counter
        end if;
    end process;

    
end Behavioral;
