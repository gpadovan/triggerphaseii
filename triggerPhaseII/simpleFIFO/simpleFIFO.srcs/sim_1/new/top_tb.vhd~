library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector

entity top_tb is
--  Port ( );
end top_tb;

architecture Behavioral of top_tb is
    
    -- input signals from test bench to top
    -- clock signals
    signal sim_clock_400MHz : std_logic;
    signal sim_clock_40MHz : std_logic;    
    -- control signals
    signal sim_reset : std_logic;
    signal sim_write_en : std_logic;
    signal sim_read_en : std_logic;
    
    -- output signals from top to test bench
    signal sim_counter : std_logic_vector (31 downto 0);
    
    -- other signals
    signal sim_hit : std_logic_vector (31 downto 0) := (others => '0');
    
    component top
        Port (clock_400MHz : in std_logic;
              clock_40MHz : in std_logic;
              reset : in std_logic;
              write_en : in std_logic;
              read_en : in std_logic;
              counter : out std_logic_vector (31 downto 0) );
    end component;
    
    
begin
    my_sim : top port map (
        clock_400MHz => sim_clock_400MHz,
        clock_40MHz => sim_clock_40MHz,
        reset => sim_reset,
        write_en => sim_write_en,
        read_en => sim_read_en,
        counter => sim_counter);
        
    -- clock generation processes  
    clock_400MHz_gen : process
    begin
        sim_clock_400MHz <= '1';
        wait for 1.25 ns;
        sim_clock_400MHz <= '0';
        wait for 1.25 ns;
    end process;
        
    clock_40MHz_gen : process
    begin
        sim_clock_40MHz <= '1';
        wait for 12.5 ns;
        sim_clock_40MHz <= '0';
        wait for 12.5 ns;
    end process;

    -- control signal generation processes
    reset_gen : process
    begin
      sim_reset <= '0';
      wait for 5 ns;
      sim_reset <= '1';
      wait for 500 ns;
      sim_reset <= '0';
      wait;
    end process;

    writeRead_en_gen : process
    begin
      sim_write_en <= '0';
      sim_read_en <= '0';
      wait for 1000 ns;
      sim_write_en <= '1';
      wait for 100 ns;
      sim_read_en <= '1';
      wait;
    end process;

    -- hit generation process
    hit_gen : process(sim_clock_400MHz)
    begin
        if rising_edge(sim_clock_400MHz) then
            sim_hit <= sim_hit + 1;
        end if;
    end process;
        
    
end Behavioral;
