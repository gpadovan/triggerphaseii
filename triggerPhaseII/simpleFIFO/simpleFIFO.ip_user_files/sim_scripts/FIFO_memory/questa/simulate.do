onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib FIFO_memory_opt

do {wave.do}

view wave
view structure
view signals

do {FIFO_memory.udo}

run -all

quit -force
