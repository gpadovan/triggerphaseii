-- simpleFIFO
-- source file: top

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operations on std_logic_vector
library xil_defaultlib; -- FIFO library
library work;
use work.all;


entity top is
    Port (clock_400MHz : in std_logic;
          clock_40MHz : in std_logic;
          reset : in std_logic;
          write_en : in std_logic;
          read_en : in std_logic;
          data_in : in std_logic_vector (31 downto 0);
          top_out: out std_logic_vector (31 downto 0);
          top_out_count : out std_logic_vector (11 downto 0) );
end top;

architecture Behavioral of top is
    signal top_bcid : std_logic_vector (11 downto 0) := (others => '0');
    signal FIFO_empty : std_logic;
    signal FIFO_full : std_logic;
    
begin
    FIFO_inst : entity work.FIFO_memory
      Port map(
        wr_en => write_en,
        rd_en => read_en,
        din => data_in,
        dout => top_out,
        rst => reset,
        clk => clock_400MHz,
        full => FIFO_full,
        empty => FIFO_empty );

    top_bcid_gen : process (clock_40MHz)
    begin
        if rising_edge(clock_40MHz) then
            top_bcid <= top_bcid + 1; -- rinomina come BCID counter (da 11 a 0)
        end if;
    end process;
    
    top_out_count <= top_bcid;
    
end Behavioral;
