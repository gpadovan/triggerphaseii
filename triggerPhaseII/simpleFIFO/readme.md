## simpleFIFO

VHDL code implementing a simple FIFO with 32-bit long hits entering and exiting.

* source file: top.vhd
* simulation file: top_tb.vhd

-------------
Author Matteo, Giovanni