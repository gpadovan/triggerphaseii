-- example_RVari
-- code containing example by RVari to define a clock signal

-- upload all necessary libraries
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library necessary to use operators

-----------------------------------------------------------------------------------------------
--  define the entity, i.e. the black box implementing the electronics, its inputs and outputs
entity top is
    -- define all iunput and output signal inside the funcion Port()
    Port (clock : in std_logic; -- input signal giving the clock
          counter : out std_logic_vector (3 downto 0) ); -- output signal giving a binary counter of alla clocks occourred since the beginning
end top;

-----------------------------------------------------------------------------------------------
-- define the architecture, i.e. the behaviour of the electonincs component
architecture Behavioral of top is
    signal counter_tmp : std_logic_vector (3 downto 0) := "0000"; -- internal variable that enables to perform adding operations

begin
    counter <= counter_tmp;

    logic : process (clock)
    begin
        if rising_edge(clock) then -- control if you are on the rising edge of the clock signal
            counter_tmp <= counter_tmp+1; -- conut the number of clocks
        end if;
    end process;

end Behavioral;
