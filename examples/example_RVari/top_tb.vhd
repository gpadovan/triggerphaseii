library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-----------------------------------------------------------------------------------------------
-- Define an ampty enthity
entity top_tb is
--  Port ( );
end top_tb;

-----------------------------------------------------------------------------------------------
-- define the architecture of test bench
architecture Behavioral of top_tb is

    signal sim_clock : std_logic;
    signal sim_counter: std_logic_vector (3 downto 0);
    
    component top                                                   -- DOMANDA: perche' e' necessario definire il componente?
        Port (clock : in std_logic;
            counter : out std_logic_vector (3 downto 0) );
    end component;

begin
    my_sim : top port map(                                          -- DOMANDA: cosa e' il port mapping?
        clock => sim_clock,                                         -- DOMANDA: perche' si utilizzano le assegnazioni =>, invece che <= ?
        counter => sim_counter);
    
    clock_gen : process
    begin
        sim_clock <= '1'; wait for 100 ns;
        sim_clock <= '0'; wait for 100 ns;
    end process;
    
end Behavioral;
