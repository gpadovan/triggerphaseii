-- example_two_clocks

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity top_tb is
--  Port ( );
end top_tb;

architecture Behavioral of top_tb is
    signal sim_clock_1 : std_logic;
    signal sim_clock_2 : std_logic;
    
    signal sim_clock_and : std_logic;
    signal sim_clock_or : std_logic;
    signal sim_clock_nand : std_logic;
    signal sim_clock_xor : std_logic;
    signal sim_clock_not : std_logic;
        
    component top
        Port (clock_1 : in std_logic;
              clock_2 : in std_logic;
              
              clock_and : out std_logic;
              clock_or : out std_logic;
              clock_nand : out std_logic;
              clock_xor : out std_logic; 
              clock_not : out std_logic );
    end component;

begin
    my_sim : top port map(
        clock_1 => sim_clock_1,
        clock_2 => sim_clock_2,
        
        clock_and => sim_clock_and,
        clock_or => sim_clock_or,
        clock_nand => sim_clock_nand,
        clock_xor => sim_clock_xor,
        clock_not => sim_clock_not );
       
   clock_gen_1 : process
   begin
       sim_clock_1 <= '1';
       wait for 100 ns;
       sim_clock_1 <= '0';
       wait for 100 ns;
   end process;

    clock_gen_2 : process
    begin
        sim_clock_2 <= '1';
        wait for 30 ns;
        sim_clock_2 <= '0';
        wait for 30 ns;
    end process;

end Behavioral;
