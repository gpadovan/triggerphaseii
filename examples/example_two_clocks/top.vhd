-- example_two_clocks

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

entity top is
    Port (clock_1 : in std_logic;
          clock_2 : in std_logic;
          
          clock_and : out std_logic;
          clock_or : out std_logic; 
          clock_nand : out std_logic; 
          clock_xor : out std_logic;
          clock_not : out std_logic );
end top;

architecture Behavioral of top is    
begin
    -- implement major logica operations with the two clocks
    clock_and <= clock_1 AND clock_2;
    clock_or <= clock_1 OR clock_2;   
    clock_nand <= clock_1 NAND clock_2;
    clock_xor <= clock_1 XOR clock_2;
    clock_not <= NOT clock_1;
    
end Behavioral;
