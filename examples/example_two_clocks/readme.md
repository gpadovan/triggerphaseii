## example_two_clocks

VHDL code that implements a circuit taking as input two clocks with different periods and releasing as output results of basic logical operations.

* source file: top.vhd
* simulation file: top_tb.vhd