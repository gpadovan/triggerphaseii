-- my_example_RVari
-- my personal re-elaboration of codce by R. Vari, implementing a counter

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

entity top is
    Port (clock : in std_logic;
          counter : out std_logic_vector ( 7 downto 0 ) );
           
end top;

architecture Behavioral of top is
    signal counter_tmp : std_logic_vector ( 7 downto 0 ) := "00000000";
    
begin
    counter <= counter_tmp;
        
    logic : process (clock)
    begin
        if rising_edge(clock) then
            counter_tmp <= counter_tmp+1;
        end if;
    end process;

end Behavioral;
