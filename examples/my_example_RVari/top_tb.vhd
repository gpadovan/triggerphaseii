library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity top_tb is
--  Port ( );
end top_tb;

architecture Behavioral of top_tb is

    signal sim_clock : std_logic;
    signal sim_counter: std_logic_vector (7 downto 0);
    
    component top
        Port (clock : in std_logic;
            counter : out std_logic_vector(7 downto 0) );
    end component;
    

begin

    my_sim : top port map(
    clock => sim_clock,
    counter => sim_counter);
        
    clock_gen : process
    begin
        sim_clock <= '1'; wait for 100 ns;
        sim_clock <= '0'; wait for 100 ns;
    end process;    

end Behavioral;
