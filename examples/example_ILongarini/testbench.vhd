library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity testbench is
--  Port ( );
end testbench;

architecture Behavioral of testbench is
  signal tb_clock : std_logic;
  signal A : std_logic;
  
begin

  tb_clock_gen : process
  begin
    wait for 12.5 ns;
    tb_clock <= '0';
    wait for 12.5 ns;
    tb_clock <= '1';
  end process;        
  
  top_inst : entity work.top
    port map (
      clock => tb_clock
      );


--A_gen : process
  --begin
  --  -- fa cose su A
  --end process;        
  
  

end Behavioral;




